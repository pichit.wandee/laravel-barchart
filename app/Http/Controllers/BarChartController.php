<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Sale;

class BarChartController extends Controller
{
    public function Barchart()
    // {
    //     $sale = Sale::select("date","seller","car")->get();

    //     $answer = ['Dates','Seller','Car'];

    //     foreach($sale as $key => $value){
    //         $answer[++$key] = [$value->date, (int)$value->seller, (int)$value->car];
    //     }
    //     return view('barchart',compact('answer'));
    // }

    ////
    {
        $sale = Sale::select("date","seller","car")->get();

        $answer[] = ['Date','Seller','Car'];

        foreach($sale as $key => $value){
            $answer[++$key] = [$value->date,(int)$value->seller, (int)$value->car];
        }
        
        return view('barchart', compact('answer'));
    }
}
