<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    
    <script type="text/javascript">
        google.charts.load('current',{'packages':['bar']});
        google.charts.setOnLoadCallback(drawChart);

        function drawChart() {
            var data = google.visualization.arrayToDataTable({{ Js::from($answer) }});
            

            
            var options = {
                chart: {
                    title: 'Retail sales',
                    subtitle: 'Retail sale of monthly',
                },
            };

            var chart = new google.charts.Bar(document.getElementById('barchart'));

            chart.draw(data, google.charts.Bar.convertOptions(options));
        }
    </script>
    <title>Bar Chart ::</title>
</head>
<body>
    <h1>Bar chart retail sale</h1>
    <div id="barchart" style="width: 1000px; height: 600px;" ></div>
</body>
</html>